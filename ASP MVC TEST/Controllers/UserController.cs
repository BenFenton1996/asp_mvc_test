﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP_MVC_TEST.Models;

namespace ASP_MVC_TEST.Controllers
{
    public class UserController : Controller
    {
        IProductService ProductService;
        IUserService UserService;

        public UserController(IProductService ProductService, IUserService UserService)
        {
            this.ProductService = ProductService;
            this.UserService = UserService;
        }

        /// <summary>
        /// Gets all Products from the Product table in the database created by the user and returns a View with a List of those Products as an argument
        /// The View returned prints all details of the Products in the List as well as buttons that allow Users to edit or delete each Product
        /// </summary>
        /// <returns>A View that uses a List of Products passed to it to print all Product details
        /// and gives options to edit or delete each Product</returns>
        [HttpGet]
        public ViewResult Index()
        {
            if (Session["Username"] == null)
            {
                return View("Login");
            }
            else
            {
                return View("Index", ProductService.ProductsByUser((String)Session["Username"]).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Allows the User to log-in by providing a View with fields for their details. If already logged in, returns a View telling them
        /// that they have already logged-in and provides navigation buttons
        /// </summary>
        /// <returns>A View with fields for logging-in, if the User is already logged in then provides a different View telling them so and
        /// provides buttons for navigation</returns>
        [HttpGet]
        public ViewResult Login()
        {
            if (Session["Username"] != null) 
            {
                return View("LoggedIn");
            }
            else
            {
                return View("Login");
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Processes log-in requests, if details are invalid then reloads the log-in page
        /// </summary>
        /// <param name="user">The user details to check against the User table in the database</param>
        /// <returns>The "Login" View if details are invalid to allow them to try logging in again, returns the UserProfile View which shows
        /// all Products created by the user if log-in is successful</returns>
        [HttpPost]
        public ViewResult Login(User User)
        {
            int Id = UserService.CheckUser(User);

            if (UserService.CheckUser(User) == 0)
            {
                ViewBag.Exists = false;
                return View("Login");
            }
            else
            {
                Session["Username"] = User.Username;
                Session["UserId"] = Id;
                return View("Index", ProductService.ProductsByUser(User.Username));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Abandons the Session to clear user log-in details, logging them out
        /// </summary>
        /// <returns>The "Login" View that has fields to allow the user to log-in</returns>
        [HttpGet]
        public ViewResult Logout()
        {
            Session.Abandon();
            return View("Login");
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets all User data from the User table in the database and returns it
        /// </summary>
        /// <returns>A List containing all Users from the User table in the database</returns>
        [HttpGet]
        public ViewResult AllUsers()
        {
            if (Session["Username"] == null)
            {
                return View("Login");
            }
            else
            {
                return View("AllUsers", UserService.GetAllUsers().OrderBy(x => x.Username));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a View with fields for allowing the User to create a new account
        /// </summary>
        /// <returns>The "NewUser" View that has fields to allow users to enter details for a new account</returns>
        [HttpGet]
        public ViewResult NewUser()
        {
            return View("NewUser");
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Processes new account requests, if the username or email are already in use returns the NewUser View with the fields 
        /// to allow the user to create an account with a different name
        /// </summary>
        /// <param name="user">The user details to put into the User table database</param>
        /// <returns>The "Login" View to allow users to log-in with their new account details, or if the account already exists
        /// returns the "NewUser" View so that the user can create an account with a different name/email</returns>
        [HttpPost]
        public ViewResult NewUser(User user)
        {
            if (ModelState.IsValid)
            {
                if(UserService.Exists(user) == true)
                {
                    ViewBag.Exists = true;
                    return View("NewUser");
                }

                UserService.NewUser(user);
                return View("Login");
            }
            else
            {
                return View("NewUser");
            }
        }
    }
}