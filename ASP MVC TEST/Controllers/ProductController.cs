﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP_MVC_TEST.Models;

namespace ASP_MVC_TEST.Controllers
{
    public class ProductController : Controller
    {
        IProductService ProductService;
        IMyProductService MyProductService;

        public ProductController(IProductService ProductService, IMyProductService MyProductService)
        {
            this.ProductService = ProductService;
            this.MyProductService = MyProductService;
        }

        /// <summary>
        /// Adds a new Product to the User's MyProducts
        /// </summary>
        /// <param name="ProductId">The Product Id to add to the User's MyProducts</param>
        /// <returns>The "MyProducts" View populated with all of the Products the User has added to their MyProducts</returns>
        [HttpGet]
        public ViewResult AddMyProducts(String ProductId)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                MyProductService.NewMyProduct((int)Session["UserId"], ProductId);
                return View("AvailableProducts", ProductService.AvailableProducts((int)Session["UserId"]).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the "AvailableProducts" View populated with Product data
        /// </summary>
        /// <returns>A View populated with Product data</returns>
        [HttpGet]
        public ViewResult AvailableProducts()
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("AvailableProducts", ProductService.AvailableProducts((int)Session["UserId"]).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Shows the User all of the Products they have added to their MyProducts
        /// </summary>
        /// <returns>The "MyProducts" View populated with all of the Products the User has added to their MyProducts</returns>
        [HttpGet]
        public ViewResult MyProducts()
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("MyProducts", MyProductService.MyProducts((int)Session["UserId"]).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Provides a page with fields to edit an existing Product. Passes the EditProduct View the ID of the Product to be edited so that
        /// it can request the Product's data to populate the fields with the existing data so that users can see what they're changing
        /// </summary>
        /// <param name="Id">The Id to search the Product table for so that the EditProduct View's fields can be populated with its data</param>
        /// <returns>The "EditProduct" View populated with fields populated with the Product's data that can be edited</returns>
        [HttpGet]
        public ViewResult EditProduct(int ProductId)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("EditProduct", ProductService.ProductsById(ProductId));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Takes the edited Product and uses it to set the values of the one in the database to reflect the changes, unless
        /// Product is malformed in which case returns the "EditProduct" View for creating a new Product
        /// </summary>
        /// <param name="product">The edited Product that is used to change the values of the one in the database</param>
        /// <returns>The "UserIndex" showing the user the index which displays all of the Products they created, unless
        /// the Product submitted was malformed in which case returns the "EditProduct" View</returns>
        [HttpPost]
        public ViewResult EditProduct(Product Product)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    ProductService.EditProduct(Product);
                    return View("../User/Index", ProductService.ProductsByUser((String)Session["Username"]));
                }
                else
                {
                    return View("EditProduct", Product);
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Deletes a Product by searching for its ID
        /// </summary>
        /// <param name="Id">The Id to check against the Product table, when it finds a match that Product is deleted</param>
        /// <returns>The "Index" View showing the user the index which displays all of the Products they created</returns>
        [HttpGet]
        public ViewResult DeleteProduct(int ProductId)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                ProductService.DeleteProduct(ProductId);
                return View("../User/Index", ProductService.ProductsByUser((String)Session["Username"]));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Deletes a MyProduct by searching for its Id
        /// </summary>
        /// <param name="Id">The Id to the MyProduct table for, when it finds a match that MyProduct is deleted</param>
        /// <returns>The "Index" View showing the user the index which displays all of the Products they created</returns>
        [HttpGet]
        public ActionResult DeleteMyProduct(int ProductId)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                MyProductService.DeleteMyProduct(ProductId, (int)Session["UserId"]);
                return RedirectToAction("MyProducts", "Product");
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets all Product data from the Product table in the database and passes it to the "AllProducts" View as an argument to populate the View
        /// with Product data
        /// </summary>
        /// <returns>The "AllProducts" View that displays the data of the List of Products passed to it</returns>
        [HttpGet]
        public ViewResult AllProducts(int Page)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                ViewBag.Page = Page;
                return View("AllProducts", ProductService.GetPage(Page));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns a View that has fields for searching for Products, allows for searching by User or Product Name
        /// </summary>
        /// <returns>The "SaerchProducts" View with fields for searching for Products by User or Product Name</returns>
        [HttpGet]
        public ViewResult SearchProducts()
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("SearchProducts");
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the Product table in the database for Products with Names matching the Query String
        /// </summary>
        /// <returns>The "AllProducts" View that displays the data of the List of Products passed to it</returns>
        [HttpGet]
        public ViewResult FindProducts(String Name)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("AllProducts", ProductService.ProductsByName(Name).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the Product table in the database for Products created by the Username in the Query String
        /// </summary>
        /// <returns>The "AllProducts" View that displays the data of the List of Products passed to it</returns>
        [HttpGet]
        public ViewResult UserProducts(String Username)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("AllProducts", ProductService.ProductsByUser(Username).OrderBy(x => x.Name));
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Returns the "AddProduct" View which has fields for creating a new Product
        /// </summary>
        /// <returns>The "AddProduct" View which has fields for creating a new Product</returns>
        [HttpGet]
        public ViewResult NewProduct()
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                return View("AddProduct");
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Processes new Product POST requests
        /// </summary>
        /// <param name="Product">The Product to add to the database</param>
        /// <returns>The "Submitted" View which shows the details of the new Product</returns>
        [HttpPost]
        public ViewResult NewProduct(Product Product)
        {
            if (Session["Username"] == null)
            {
                return View("../User/Login");
            }
            else
            {
                if (ModelState.IsValid)
                {
                    ProductService.NewProduct(Product);
                    return View("Submitted", Product);
                }
                else
                {
                    return View("AddProduct");
                }
            }
        }
    }
}