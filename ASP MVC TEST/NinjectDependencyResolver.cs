﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ASP_MVC_TEST.Models;
using Ninject;

namespace ASP_MVC_TEST
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        public IKernel Kernel;

        public NinjectDependencyResolver(IKernel Kernel)
        {
            this.Kernel = Kernel;
            AddBindings();
        }

        public object GetService(Type ServiceType)
        {
            return Kernel.TryGet(ServiceType);
        }

        public IEnumerable<object> GetServices(Type ServiceType)
        {
            return Kernel.GetAll(ServiceType);
        }

        public void AddBindings()
        {
            Kernel.Bind<IProductService>().To<ProductService>();
            Kernel.Bind<IMyProductService>().To<MyProductService>();
            Kernel.Bind<IUserService>().To<UserService>();
        }
    }
}