﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_MVC_TEST.Models
{
    public interface IMyProductService
    {
        void NewMyProduct(int UserId, String ProductIdString);
        //Adds a new MyProduct with UserId and ProductId set by the parameters

        IEnumerable<Product> MyProducts(int UserId);
        //Gets all MyProducts with a UserId matching the parameter

        void DeleteMyProduct(int ProductId, int UserId);
        //Deletes a Product with ProductId and UserId matching the parameters

        MyProduct SearchMyProduct(int ProductId, int UserId);
        //Searches for a Product with ProductId and UserId matching the parameters
    }
}
