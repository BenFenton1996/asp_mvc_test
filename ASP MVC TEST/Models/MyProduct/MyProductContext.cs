﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using ASP_MVC_TEST.Models;

namespace ASP_MVC_TEST.Models
{
    public class MyProductContext : DbContext
    {
        public DbSet<MyProduct> MyProduct { get; set; }
    }
}