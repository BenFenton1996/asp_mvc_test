﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_MVC_TEST.Models
{
    public class MyProductService : IMyProductService
    {
        /// <summary>
        /// Adds a new MyProduct to the MyProduct table in the database
        /// </summary>
        /// <param name="UserId">The UserId that will be used as the MyProduct's MyId</param>
        /// <param name="ProductId">The ProductId that will be used as the MyProduct's ProductId</param>
        public void NewMyProduct(int UserId, String ProductIdString)
        {
            using (MyProductContext db = new MyProductContext())
            {
                int ProductId = Int32.Parse(ProductIdString);
                MyProduct MyProduct = new MyProduct { UserId = UserId, ProductId = ProductId };


                if ((db.MyProduct.Any(x => x.UserId == UserId)) && (db.MyProduct.Any(x => x.ProductId == ProductId)))
                {
                    return;
                }

                db.MyProduct.Add(MyProduct);
                db.SaveChanges();
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets all of the User's MyProducts and returns them in a List
        /// </summary>
        /// <param name="UserId">The UserId to check against the MyProduct's MyId</param>
        /// <returns>A List containing all of the User's MyProducts</returns>
        public IEnumerable<Product> MyProducts(int UserId)
        {
            using (MyProductContext db = new MyProductContext())
            {
                List<Product> data = new List<Product>();

                using (ProductContext ProductDb = new ProductContext())
                {
                    foreach (MyProduct MyProducts in db.MyProduct)
                    {
                        if (MyProducts.UserId == UserId)
                        {
                            data.Add(ProductDb.Products.Find((MyProducts.ProductId)));
                        }
                    }
                }
                return data;
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the MyProducts table in the database for a MyProduct with a ProductId matching the Id parameter and deletes it
        /// </summary>
        /// <param name="Id">The Id to search the MyProducts table in the database for</param>
        /// <param name="UserId">The UserId to search the MyProducts table in the database for</param>
        public void DeleteMyProduct(int ProductId, int UserId)
        {
            using (MyProductContext db = new MyProductContext())
            {
                MyProduct MyProduct = SearchMyProduct(ProductId, UserId);

                if (MyProduct != null)
                {
                    db.MyProduct.Attach(MyProduct);
                    db.MyProduct.Remove(MyProduct);
                    db.SaveChanges();
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the MyProducts table in the database for a MyProduct with a ProductId and UserId matching the parameters
        /// </summary>
        /// <param name="ProductId">The ProductId to check against the MyProducts in the table</param>
        /// <param name="UserId">The UserId to check against the MyProducts in the table</param>
        /// <returns>A MyProduct with ProductId and UserId matching the parameters</returns>
        public MyProduct SearchMyProduct(int ProductId, int UserId)
        {
            using (MyProductContext db = new MyProductContext())
            {
                foreach (MyProduct MyProduct in db.MyProduct)
                {
                    if ((MyProduct.UserId == UserId) && (MyProduct.ProductId == ProductId))
                    {
                        return MyProduct;
                    }
                }
                return null;
            }
        }
    }
}