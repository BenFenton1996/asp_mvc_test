﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_MVC_TEST.Models
{
    public interface IProductService
    {
        IEnumerable<Product> AvailableProducts(int UserId);
        //Returns a List of all Products the User hasn't added to their MyProducts

        IEnumerable<Product> GetAll();
        //Returns a List of all Products

        IEnumerable<Product> GetPage(int Page);
        //Returns a List of Products for a page

        IEnumerable<Product> ProductsByUser(String Username);
        //Returns all Products created by a specific User

        IEnumerable<Product> ProductsByName(String Name);
        //Returns all Products with Names that match the Name parameter

        Product ProductsById(int Id);
        //Returns all Products with Ids that match the Id parameter

        void NewProduct(Product Product);
        //Adds a new Product

        void DeleteProduct(int Id);
        //Deletes a Product

        void EditProduct(Product Edit);
        //Edits a Product
    }
}
