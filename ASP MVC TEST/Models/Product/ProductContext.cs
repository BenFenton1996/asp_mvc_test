﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using ASP_MVC_TEST.Models;

namespace ASP_MVC_TEST.Models
{
    public class ProductContext : DbContext
    {
        /// <summary>
        /// The Product Context used to interact with the Product table in the database
        /// </summary>
        public DbSet<Product> Products { get; set; }
    }
}