﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ASP_MVC_TEST.Models
{
    public class Product
    {
        [Key]
        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please enter a product name")]
        public String Name { get; set; }

        [Required(ErrorMessage = "Please enter a product type")]
        public String Type { get; set; }

        [Required(ErrorMessage = "Please enter a valid price")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "Please enter a valid price")]
        public String Price { get; set; }

        [Required(ErrorMessage = "Please enter a valid username")]
        public String Username { get; set; }

        [Required]
        public String UserId { get; set; }
    }
}