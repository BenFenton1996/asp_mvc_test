﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP_MVC_TEST.Models
{
    public class ProductService : IProductService
    {
        /// <summary>
        /// Creates two Product Lists containing User Products and all Products then checks User Products against all Products and removes duplicates
        /// </summary>
        /// <param name="UserId">The UserId to use to get their Products</param>
        /// <returns>A List of all Products minus ones the User has added to their MyProducts</returns>
        public IEnumerable<Product> AvailableProducts(int UserId)
        {
            using (ProductContext db = new ProductContext())
            {
                MyProductService MyProductService = new MyProductService();
                IEnumerable<Product> MyProducts = MyProductService.MyProducts(UserId);
                List<Product> Products = db.Products.ToList();

                foreach (Product Product in MyProducts)
                {
                    Products.RemoveAt(Products.FindIndex(x => x.Id == Product.Id));
                }

                return Products;
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets all Products from the database, puts them into a List and returns it
        /// </summary>
        /// <returns>A Product List containing all Products from the Product table database</returns>
        public IEnumerable<Product> GetAll()
        {
            using (ProductContext db = new ProductContext())
            {
                return db.Products;
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets all Products from the database, puts them into a List and returns it
        /// </summary>
        /// <returns>A Product List containing all Products from the Product table database</returns>
        public IEnumerable<Product> GetPage(int Page)
        {
            using (ProductContext db = new ProductContext())
            {
                return db.Products.OrderBy(x => x.Id).Skip(9 * Page).Take(9).ToList();
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks the Username parameter against Product Usernames to find Products created by that User
        /// </summary>
        /// <returns>A Product List containing all Product data created by the specified User</returns>
        public IEnumerable<Product> ProductsByUser(String Username)
        {
            using (ProductContext db = new ProductContext())
            {
                return db.Products.Where(x => x.Username.Equals(Username)).ToList();
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks the Name parameter against Product Names to find Products with that Name
        /// </summary>
        /// <returns>A Product List containing all Product data created by the specified User</returns>
        public IEnumerable<Product> ProductsByName(String Name)
        {
            using (ProductContext db = new ProductContext())
            {
                return db.Products.Where(x => x.Name.Contains(Name)).ToList();
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the Products table in the database for a Product with an Id matching the Id parameter and then returns it
        /// </summary>
        /// <returns>The Product with a matching ID</returns>
        public Product ProductsById(int Id)
        {
            using (ProductContext db = new ProductContext())
            {
                return db.Products.Find(Id);
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// If the Product is not null, adds a new Product to the database
        /// </summary>
        /// <param name="product">The Product to add to the database</param>
        public void NewProduct(Product Product)
        {
            using (var db = new ProductContext())
            {
                if (Product != null)
                {
                    db.Products.Add(Product);
                    db.SaveChanges();
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Searches the Products table in the database for a Product with an Id matching the Id parameter and deletes it
        /// </summary>
        /// <param name="Id">The Id to search the Products table in the database for</param>
        public void DeleteProduct(int Id)
        {
            using (ProductContext db = new ProductContext())
            {
                Product Product = new Product { Id = Id };
                db.Products.Attach(Product);
                db.Products.Remove(Product);
                db.SaveChanges();
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Replaces the values of a Product in the database with the values of the Product parameter edit which has the same Id
        /// </summary>
        /// <param name="edit">The edited Product</param>
        public void EditProduct(Product Edit)
        {
            using (ProductContext db = new ProductContext())
            {
                if (Edit != null)
                {
                    Product original = db.Products.SingleOrDefault(x => x.Id == Edit.Id);
                    if (original != null)
                    {
                        original.Name = Edit.Name;
                        original.Price = Edit.Price;
                        original.Type = Edit.Type;
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}