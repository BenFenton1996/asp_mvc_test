﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;
using ASP_MVC_TEST.Models;

namespace ASP_MVC_TEST.Models
{
    public class UserContext : DbContext
    {
        /// <summary>
        /// The User Context used to interact with the User table in the database
        /// </summary>
        public DbSet<User> Users { get; set; }
    }
}