﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace ASP_MVC_TEST.Models
{
    public class UserService : IUserService
    {
        /// <summary>
        /// Checks if the user already exists in the database and adds them to it if they do not, hashes password for security
        /// </summary>
        /// <param name="user">The User to check against the database and to add if it doesn't already exist</param>
        public void NewUser(User User)
        {
            using (UserContext db = new UserContext())
            {
                if (User != null)
                {
                    if (Exists(User) == true)
                    {
                        return;
                    }
                    else
                    {
                        User.Password = HashPass(User.Password);
                        db.Users.Add(User);
                        db.SaveChanges();
                    }
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Check if the user already exists in the database
        /// </summary>
        /// <param name="user"></param>
        /// <returns>True if the user exists, false if they do not</returns>
        public Boolean Exists(User User)
        {
            using (UserContext db = new UserContext())
            {
                if (User != null)
                {
                    if ((db.Users.Any(x => x.Username == User.Username)) || (db.Users.Any(x => x.Email == User.Email)))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Populates a List with all User data from the User table in the database and returns it
        /// </summary>
        /// <returns>A User List containing all Users from the User table in the database</returns>
        public IEnumerable<User> GetAllUsers()
        {
            using (UserContext db = new UserContext())
            {
                return db.Users.ToList();
            }
        }

        //--------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Searches the User table in the database for a User with a Username matching the parameter and returns their data
        /// </summary>
        /// <param name="Username">The Username to check against Usernames in the User table</param>
        /// <returns>An instance of a User with a Username matching the parameter</returns>
        public User UserById(int Id)
        {
            using (UserContext db = new UserContext())
            {
                User User = db.Users.Find(Id);
                return (User);
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Hashes the user password with SHA256 for security
        /// </summary>
        /// <param name="Password">The password to hash</param>
        /// <returns>The hashed password as a String</returns>
        public String HashPass(String Password)
        {
            using (SHA256 hasher = new SHA256Managed())
            {
                byte[] passData = Encoding.UTF8.GetBytes(Password);
                byte[] hash = hasher.ComputeHash(passData);
                Password = BitConverter.ToString(hash).Replace("-", "");
                return Password;
            }
        }

        //--------------------------------------------------------------------------------------------

        /// <summary>
        /// Checks User details after a log-in request to see if their account exists
        /// </summary>
        /// <param name="user">The User to check against the database</param>
        /// <returns>A boolean that is false if the account exists and true if it does not</returns>
        public int CheckUser(User User)
        {
            using (UserContext db = new UserContext())
            {
                if (User != null)
                {
                    foreach (User Check in db.Users)
                    {
                        if ((User.Username.Equals(Check.Username)) && (HashPass(User.Password).Equals(Check.Password)) && (User.Email.Equals(Check.Email)))
                        {
                            return Check.Id;
                        }
                    }
                }
            }
            return 0;
        }
    }
}