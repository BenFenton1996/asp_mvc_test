﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASP_MVC_TEST.Models
{
    public interface IUserService
    {
        void NewUser(User User);
        //Adds a new User

        Boolean Exists(User User);
        //Checks if a User exists

        IEnumerable<User> GetAllUsers();
        //Returns all Users

        User UserById(int Id);
        //Returns a User with Id matching the parameter

        String HashPass(String Password);
        //Returns the hashed parameter

        int CheckUser(User User);
        //Check the User details to see if they match an existing User
    }
}
